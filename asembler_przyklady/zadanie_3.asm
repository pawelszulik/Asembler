format PE Console 4.0
entry start
include 'include/win32ax.inc'
include 'win_macros.inc'
include 'tablice.inc'
include 'proc.inc'

section '.text' code readable executable
start:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;obliczanie los;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;los = (stala_a * los + stala_b) % stala_m
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; inicjalizujemy koprocesor
        finit
        ; czyscimy eax
        xor eax, 0f
        ; pobieramy zegar
        rdtsc
        ; do los przenosimy eax (wystarczy taka duza liczba)
        mov [los], eax
        ; na koprocesory dajemy stala A
        fild [stala_a]
        ; na koprocesor dajemy wylosowana liczbe (z zegara)
        fild [los]
        ; mnozymy a*los
        fmulp         ; na stosie mamy wynik
        ; dodajemy na stos b
        fild [stala_b]
        ; dodajemy b do wyniku
        faddp        ; na stosie mamy wynik
        ; na stos przenosimy m
        fild [stala_m]
        ; dzielimy przez m wynik
        fdivp
        ; kopiujemy wynik do zmiennej los i zdejmujemy z stosu
        fstp [los]


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;obliczanie wartosc_los;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;wartosc_los = (los % (L2 - L1 + 1)) + L1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; dodajemy na stos L1
        fild [L2]
        ; dodajemy na stos L2
        fild [L1]
        ; odejmujemy L2 - L1
        fsubp
        ; dodajemy na stos jedynke
        fld1
        ; dodajemy wynik do 1
        faddp
        ; dodajemy na stos los
        fild [los]
        ; dzielimy los przez wynik
        fdivp
        ; dodajemy na stos L1
        fild [L1]
        ; wykonujemy dodawanie
        faddp
        ; zdejmujemy ze stosu do wartosc_los wynik
        fstp [wartosc_los]


        pob_znak

        end_prog

section '.data' data readable writeable
        los             dd 0
        wartosc_los     dd 0
        stala_a         dd 13100233
        stala_b         dd 11040857
        stala_m         dd 9999991
        L1              dw 0
        L2              dw 99
        Xmin            dw 1.3 ; trzeba z tego zrobic minus
        Xmax            dw 0.6
        Ymin            dw 0.9 ; trzeba z tego zrobic minus
        Ymax            dw 0.45



        zmienna         dq 0
        iloscPowtorzen  dd 10
        sto     dw 100

        tablica dd 5, 4, 3, 2, 1                 ; tablica bajtów
        tablicaSlow dw 5, 4, 3, 2, 1                 ; tablica slow

        ; deklaracja talicy 10 na 20 z wypelnionymi 0
        tab: TIMES  100      dd      0






; POMOC
;
; db - byte        - bajt            - 1 bajt               - max wartosc 255
; dw - word        - slowo           - 2 bajty              - max wartosc 65 535
; dd - dword       - podwojne slowo  - 4 bajty       int    - max wartosc 4 294 967 295
; pw - pword       - potrojne slowo  - 6 bajtow             - max wartosc w hex FFFF FFFF FFFF - 281 474 976 710 655
; dq - qword       - poczworne slowo - 8 bajtow      long
; td - tbyte       -                 - 10 bajtow
;
;fadd st0, st1 to samo co faddp
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
