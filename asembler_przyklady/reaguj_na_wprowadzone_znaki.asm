format PE Console 4.0
entry start
include 'include/win32ax.inc'
include 'win_macros.inc'

section '.text' code readable executable
start:
        clrscr    ; czyscimy ekran

        ustaw_kursor 0,5
        cinvoke printf,txtHello

        ustaw_kursor 1,5
        cinvoke printf, txtPrzepisz


        ustaw_kursor 2,5
        cinvoke scanf, '%x', txtWprowadzony
        mov [txtWprowadzony], edx


        mov eax,[txtPrzepisz]
        mov edx,[txtWprowadzony]
        cmp eax, edx
        JNE  FAIL

        ustaw_kursor 3,5
        cinvoke printf,txtGratulacje

        JMP KONIEC

FAIL:
        ustaw_kursor 3,5
        cinvoke printf,txtFail

KONIEC:

        pob_znak
        end_prog

section '.data' data readable writeable

        txtWprowadzony dd      '',0
        txtPrzepisz    dd      'Al a',0
        txtHello       db      'Witaj, twoim zadaniem jest przepisanie ciagu znakow, wpisz tekst: ',0
        Przepisz       dd      ?,'',0;10,13,NULL
        txtGratulacje  db      'Udalo sie przepisac poprawnie :)',0
        txtFail        db      'Nie udalo sie przepisac poprawnie :(',0