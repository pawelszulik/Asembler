format PE Console 4.0
entry start
include 'include/win32ax.inc'
include 'win_macros.inc'

section '.text' code readable executable
start:
         cinvoke printf, <"Podaj wartosc a = ">
         cinvoke scanf, <"%lf">, zmienna_a
         cinvoke printf, <"Podaj wartosc b = ">
         cinvoke scanf, <"%lf">, zmienna_b
         cinvoke printf, <"Podaj wartosc n = ">
         cinvoke scanf, <"%lu">, zmienna_n
        ; jesli chcemy pobrac/wyswietlic liczbe ósemkową wpisujemy %o,
        ; jesli chcemy pobrac/wyswietlic liczbe dziesietna wpisujemy %u
        ; jesli chcemy pobrac/wyswietlic liczbe szesnastkowa wpisujemy %x
        mov eax, dword[zmienna_n]

        finit                   ; inicjalizacja koprocesora
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;obliczanie dx;;;;;dx = (b - a) / n;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        fld [zmienna_b]                      ; Dodajemy na stos liczbe b
        fld [zmienna_a]                      ; Dodajemy na stos a

        ; double x = a;
        ; wykonujemy to polecenie poniewaz okreslamy pkt od ktorego bedziemy liczyc
        fst [zmienna_x]                      ; przenosimy sobie z st0 do x wartosc poczatkowa

        fsubp                                ; dodajemy 2 liczby
        fld [zmienna_n]                      ; dodajemy na stos n
        fdivp                                ; dzielimy (a+b)/n
        fstp [zmienna_dx]                    ; przenosi z st0 do zmiennej dx jest on potrzebny do dodawania

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;obliczanie ilosci petli;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        fild [zmienna_n]                     ; dodajemy na stos zmienna n w celu konwersji jej na liczbe bez przecinka
        fistp [zmienna_n]                    ; przenosi z st0 do zmiennej licznik konwertuje na int i zdejmuje z stosu
        mov ecx, dword [zmienna_n]           ; przenosimy z  od ecx ktory jest licznikiem petli
        dec ecx                              ; zmiejszamy licznik o 1  bo jest n-1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;obliczanie sumy;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;for (int i = 1; i < n; i++)
;;;;;;;;;;;;;;;;;;;;;;;;{
;;;;;;;;;;;;;;;;;;;;;;;;;;;;x += dx;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;wynik += func(x);
;;;;;;;;;;;;;;;;;;;;;;;;}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                                             ; stos jest pusty
    petla:                                   ; petla wykonuje sie n-1
        fld [zmienna_x]                      ; przenosimy na stos x
        fld [zmienna_dx]                     ; przenosimy dx na stos
        faddp                                ; dodajemy x do dx
        fst [zmienna_x]                      ; przenosimy do zmiennej x wartosc w st0
        fptan  ;Math.Tan(x);                 ; obliczamy funkcje
        ; tutaj w ollydb nie jest liczony tangens tak sie dzieje w tylko petli
        fld [wynik]                          ; przenosimy na stos zmienna wynik
        faddp                                ; dodajemy zmienna wynik do wyniku funkcji
        fstp [wynik]                         ; zdejmujemy z st0 wynik i dajemy go do zmiennej wynik
    loop petla


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;obliczanie dx;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;wynik = (((func(a) + func(b)) / 2) + wynik)* dx;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        fld [zmienna_a]                      ; kladziemy na stos a
        fptan   ;Math.Tan(x);                ; obliczamy funkcje z a
        ; w tym miejscu na stosie w st0 jest 1.0 a w st1 mamy wynik, w dokumentacji ani w ksiazkach nie jest jasno wytlumaczone co to oznacza
        fld [zmienna_b]                      ; dajemy na stos b
        fptan   ;Math.Tan(x);                ; obliczamy funkcje z b
        ; w tym miejscu na stosie w st0 jest 1.0 a w st1 mamy wynik, w dokumentacji ani w ksiazkach nie jest jasno wytlumaczone co to oznacza
        faddp                                ; dodajemy funkcje z a i funkcje z b
        fild [wartosc_2]                     ; przenosimy 2 na stos
        fdivp                                ; dzielimy wynik funkcji przez 2
        fld [wynik]                          ; kladziemy wynik na stos
        faddp                                ; dodajemy wynik z dzielenia funkcji obliczonej wyzej do wyniku
        fld [zmienna_dx]                     ; kladziemy na stos
        fmulp                                ; mnozymy

        fstp [wynik]                         ; zdejmujemy z stosu wynik

        cinvoke printf, <"Wynik = %.3f",10,13>, dword [wynik+0], dword[wynik+4]


        pob_znak

        end_prog


section '.data' data readable writeable
        zmienna_a  dq 0.00
        zmienna_b  dq 0.00
        zmienna_n  dd 0
        zmienna_dx dq 0.0
        zmienna_x  dq 0.0

        wartosc_2 dd 2
        wynik  dq 0.00

; POMOC
;
; db - byte        - bajt            - 1 bajt               - max wartosc 255
; dw - word        - slowo           - 2 bajty              - max wartosc 65 535
; dd - dword       - podwojne slowo  - 4 bajty       int    - max wartosc 4 294 967 295
; pw - pword       - potrojne slowo  - 6 bajtow             - max wartosc w hex FFFF FFFF FFFF - 281 474 976 710 655
; dq - qword       - poczworne slowo - 8 bajtow      long
; td - tbyte       -                 - 10 bajtow
;
;fadd st0, st1 to samo co faddp
;
;
;
;
