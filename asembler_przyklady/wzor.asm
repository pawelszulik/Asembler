format PE Console 4.0
entry start
include 'include/win32ax.inc'
include 'win_macros.inc'
include 'tablice.inc'
include 'proc.inc'

section '.text' code readable executable
start:
;-----------------------------------------------------------------------------------------------
;---------Wczytywanie liczb---------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
;        clrscr
;        ustaw_kursor 0,5
        ;cinvoke printf, <'Pobierz dane do zmiennej dd o nazwie zmienna = ',0>
        ;cinvoke scanf, '%lx', zmienna

;-----------------------------------------------------------------------------------------------
;---------Tablice-------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ;tablice

        ;call procedurka
        ; pobieramy zegar
        rdtsc
        ; czyscimy edx
        AND EDX, 0h
        ; czyscimy ebx
        AND EBX, 0h
        ; bedziemy robic modulo 100 wiec do bx dajemy 100
        mov bx, 100
        ; wykonujemy dzielenie ax z bx
        div bx
        ; w dx mamy reszte
        mov [random], dx

        pob_znak

        end_prog

section '.data' data readable writeable
        random          dw 0
        zmienna         dq 0
        iloscPowtorzen  dd 10
        sto     dw 100

        tablica dd 5, 4, 3, 2, 1                 ; tablica bajtów
        tablicaSlow dw 5, 4, 3, 2, 1                 ; tablica slow

        ; deklaracja talicy 10 na 20 z wypelnionymi 0
        tab: TIMES  100      dd      0






; POMOC
;
; db - byte        - bajt            - 1 bajt               - max wartosc 255
; dw - word        - slowo           - 2 bajty              - max wartosc 65 535
; dd - dword       - podwojne slowo  - 4 bajty       int    - max wartosc 4 294 967 295
; pw - pword       - potrojne slowo  - 6 bajtow             - max wartosc w hex FFFF FFFF FFFF - 281 474 976 710 655
; dq - qword       - poczworne slowo - 8 bajtow      long
; td - tbyte       -                 - 10 bajtow
;
;fadd st0, st1 to samo co faddp
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
