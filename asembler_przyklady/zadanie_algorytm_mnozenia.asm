format PE Console 4.0
entry start
include 'include/win32ax.inc'
;include 'bibl/incl/dosbios/fasm32/std_bibl.inc'
;include 'include/win64ax.inc'
include 'win_macros.inc'
;include 'argorytmMnozenia.inc'
include 'alg.inc'

section '.text' code readable executable
start:
;-----------------------------------------------------------------------------------------------
;---------Wczytywanie liczb---------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        clrscr
        ustaw_kursor 1,5
        cinvoke printf, <'zm1 = ',0>
        cinvoke scanf, '%x', zm1
        ustaw_kursor 2,5
        cinvoke printf, <'zm2 = ',0>
        cinvoke scanf, '%x', zm2
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------


;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        mov DWORD [wynik],  0            ; zerujemy wynik, bo moga byc smieci :D
        mov WORD [licznikPetli1], 0
        mov WORD [licznikPetli2], 0

        mov ecx, 100


    petla1:


        petla2:

                alg zm1, zm2, licznikPetli1,licznikPetli2

                add [licznikPetli1],4

                mov ebx,30
                cmp ebx,[licznikPetli1]

                JAE wyjdzZPetli2

        loop petla2
wyjdzZPetli2:




        add [licznikPetli2],4
        mov WORD [licznikPetli1], 0

         mov ebx,30

         cmp ebx,[licznikPetli1]

         JAE wyjdzZGlownePetli


        loop petla1

wyjdzZGlownePetli:

        mov eax, dword [wynik]

        ustaw_kursor 4,5
        cinvoke printf, <'wynik = %x',0>, eax

        ;cinvoke printf, <'wynik = %x',0>, dword [wynik]

;-----------------------------------------------------------------------------------------------
;---------SPRAWDZENIE bez znaku-----------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ; sprawdzenie
        mov edx, [zm2]
        mov eax, [zm1]
        mul edx

        mov  dword [wynik], eax
        mov  dword [wynik+32], edx
        ustaw_kursor 5,5
        cinvoke printf, <'sprawdzenie eax= %x',0>, eax

        ustaw_kursor 6,5
        cinvoke printf, <'sprawdzenie wynik = %x',0>,edx
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------



        pob_znak

        end_prog

section '.data' data readable writeable

        zm1             dd 0
        zm2             dd 0

        licznikPetli1   dd 0

        licznikPetli2   dd 0
        wynik           dq 0

