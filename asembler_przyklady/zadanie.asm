format PE Console 4.0
entry start
include 'include/win32ax.inc'
include 'win_macros.inc'
include 'alg.inc'

section '.text' code readable executable
start:
;-----------------------------------------------------------------------------------------------
;---------Wczytywanie liczb---------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        clrscr
        ustaw_kursor 1,5
        cinvoke printf, <'zm1 = ',0>
        cinvoke scanf, '%x', zm1
        ustaw_kursor 2,5
        cinvoke printf, <'zm2 = ',0>
        cinvoke scanf, '%x', zm2
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ; do debuga

        mov esi,1
        mov esi,1
        mov esi,1
        mov esi,1

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;-----------------------------------------------------------------------------------------------
;---------sprawdzamy po pierwszym bicie od lewej jaki jest to znak i wypisujemy-----------------
;-----------------------------------------------------------------------------------------------
        mov eax, dword [zm1]
        mov edx, dword [zm2]

        bt eax,31
        jnc skokGdyzm1Ujemna
        bt edx,31
        jnc Ujemna
        jmp Dodatnia

skokGdyzm1Ujemna:
        bt edx,31
        jnc Dodatnia
Dodatnia:
        ustaw_kursor 4,4
        cinvoke printf, '+'
        mov [znak],1
        jmp Dalej
Ujemna:
        ustaw_kursor 4,4
        cinvoke printf,'-'
        mov [znak], 0

Dalej:

;-----------------------------------------------------------------------------------------------
;---------kasowanie znaku z pierwszego bita od lewej -------------------------------------------
;-----------------------------------------------------------------------------------------------
        mov eax, dword [zm1]
        mov edx, dword [zm2]
        btr eax,31
        btr edx,31
        mov dword [zm1],eax
        mov dword [zm2],edx
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        mov DWORD [wynik],  0            ; zerujemy wynik, bo moga byc smieci :D
        mov WORD [licznikPetli1], 0
        mov WORD [licznikPetli2], 0

        mov ecx, 100                     ; licznik petli na 100, tak by bylo ;]

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ; do debuga

        mov esi,1
        mov esi,1
        mov esi,1
        mov esi,1

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        mov ebx,30                                  ; do nie uzywanego przeze mnie rejestru przesuwamy 30  przyda sie do porownywania w petli

    petla1:                                                 ; pierwsza petla


        petla2:                                             ; druga petla

                alg zm1, zm2, licznikPetli1,licznikPetli2   ; odpalamy algorytm obliczania

                add [licznikPetli1],4                       ; dodajemy do licznika 4

                cmp ebx,[licznikPetli1]                     ; porownujemy czy czasem nie przekroczylo 30

                JAE wyjdzZPetli2                            ; jesli przekroczylo 30 wychodzimy z petli

        loop petla2                                         ; koniec petli2
wyjdzZPetli2:                                               ; tutaj wychodzimy z petli 2

        add [licznikPetli2],4                               ; do drugiego licznika dodajemy 4
        mov WORD [licznikPetli1], 0                         ; zerujemy pierwszy licznik petli

         cmp ebx,[licznikPetli2]                            ; porownujemy czy licznik petli czasem nie jest wiekszy od 30

         JAE wyjdzZGlownePetli                              ; jesli wiekszy od 30 wychodzimy z glownej petli


        loop petla1                                         ; koniec duze petli

wyjdzZGlownePetli:                                          ; wychodzimy tutaj z duzej petli

;-----------------------------------------------------------------------------------------------
;---------USTANIENIE ZNAKU LICZBY---------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        cmp [znak],0    ; zero to ujemna
        je dodajZnak    ; jesli jest rowny 0
        jmp Kontynuuj

dodajZnak:

        bts dword[wynik],63      ; ustawiamy w wyniku najstarszy bit na 1

Kontynuuj:

        ustaw_kursor 4,5
        cinvoke printf, <'wynik = %llx',0>,dword [wynik], dword [wynik+4]           ; wyswietlamy wynik

;-----------------------------------------------------------------------------------------------
;---------SPRAWDZENIE bez znaku-----------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ; sprawdzenie
        mov edx, dword [zm2]
        mov eax, dword [zm1]
        mul edx

        ; tutaj by trzeba bylo zrobic sprawdzenie czy liczba jest jest ujemna czy nie, jesli jest to trzeba dac bts edx,31

        ustaw_kursor 7,5
        cinvoke printf, <'wynik sprawdzajacy = %llx',0>, eax, edx
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ustaw_kursor 8,5


        pob_znak

        end_prog

section '.data' data readable writeable

        zm1             dq 0
        zm2             dq 0

        licznikPetli1   dd 0

        licznikPetli2   dd 0
        wynik           dq 0

        znak            db 0     ; zero to ujemna

