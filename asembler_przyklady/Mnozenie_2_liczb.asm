format PE Console 4.0

entry start
include 'include/win32a.inc'
include 'include/win_macros.inc'
include 'include/macro/import32.inc'

section '.text' code readable executable
start:

;wyswietlenie pierwszego tekstu
        push txtHello
        call wypisz
        ;

;czekanie na wcisniecie klawisza

;wciśniętego klawisza zwrócony zostaje w rejestrze AL)
        ;pob_znak_ECHO


        mov dword [esp],chrarr
        call [scanf]
        mov dword [esp],msg2
        call [printf]

        mov ecx,    liczba


;wyswietlenie koncowego tekstu
        push  txtPustaLinia
        call wypisz

        push txtEnd
        call wypisz

        pob_znak
;zakonczenie programu
        end_prog



proc wypisz ,txt
    wyswietl [txt]
    ret
endp




section '.idata' data readable writeable

        liczba dd 1125, 0  ; 2 * 4 bajty

        txtHello       db      'Czesc',10,13,NULL
        ;txt2          db      'Tekst 2',10,13,NULL
        txtPustaLinia  db      ' ',10,13,NULL
        txtEnd         db      'Nacisnij klawisz by zakonczyc ...',10,13,NULL
        msg2 db "Hello %s",0dh,0ah,0
        ;chrarr db "%s",0
;section '.idata' import data readable writeable
;        library msvcrt,'MSVCRT.DLL'
;        import msvcrt,
;        printf, 'printf'
;        _getch, '_getch'