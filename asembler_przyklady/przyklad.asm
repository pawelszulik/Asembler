format PE Console 4.0
entry start
include 'include/win32ax.inc'
include 'win_macros.inc'

section '.text' code readable executable
start:

        clrscr
        ustaw_kursor 1,5
        cinvoke printf, <'zm1 = ',0>
        cinvoke scanf, '%x', zm1
        ustaw_kursor 2,5
        cinvoke printf, <'zm2 = ',0>
        cinvoke scanf, '%x', zm2

        ustaw_kursor 3,5
        ;mov eax, dword [zm2]
        ;cinvoke printf, <'test = %x',0>, ax

        jns dodatni
        jmp ujemny
ujemny:
        cinvoke printf, 'ujemny '
        jmp dalej

dodatni:
        cinvoke printf, 'dodatni '
;
dalej:

        ustaw_kursor 4,5
        cinvoke printf, <'wynik = %x',0>, eax

        pob_znak

        end_prog

section '.data' data readable writeable

        zm1             dd 0
        zm2             dd 0
        wynik           dq 0

