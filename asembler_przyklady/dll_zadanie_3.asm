format PE GUI 4.0 DLL

include 'win32a.inc'

; funkcje eksportowane oraz wewn�trzne
section '.text' code readable executable



; void dllOK (void)
proc dllOK stdcall
	invoke MessageBoxA, HWND_DESKTOP,_tresc,_tytul, MB_OK
	ret
endp



proc Los stdcall, a:DWORD, los:DWORD, b:DWORD, n:DWORD
local temp:DWORD
	finit
	fild [a]
	fild [los]
	fmulp
	fild [b]
	faddp
	fild [n]
	fdivp
	fist [temp]
	mov eax, [temp]

	ret
endp

proc LosDziel stdcall, los:DWORD
local temp:DWORD

	and edx,0h
	mov eax, [los]
	mov edx, 64h
	div edx
	;and EAX, 0h
	mov eax, edx

	ret
endp

section '.data' data readable writeable
	   _tytul db "OK", 0
	   _tresc db "Biblioteka MojaDLL.dll jest dost�pna", 0




section '.idata' import data readable writeable

 library user32,'USER32.DLL'

 import  user32,\
   MessageBoxA,'MessageBoxA'


section '.edata' export data readable

  export 'MojaDLL.DLL',\
	 dllOK,'dllOK',\
	 Los,'Los',\
	 LosDziel, 'LosDziel'


section '.reloc' fixups data readable discardable
