macro tablice
{
        pushad   ;odkłada wszystkie rejestry na stoss

;-----------------------------------------------------------------------------------------------
;---------WYSWIETLENIE ADRESU PAMIECI TABLICY---------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ustaw_kursor 4,5
        cinvoke printf, <'adres tablicy = %x',0>, tab
;-----------------------------------------------------------------------------------------------
;---------PRZYPISANIE WARTOSCI DO TABLICY-------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        mov DWORD [tab],2342231234
        mov DWORD [tab+4],2126894512
        mov dword [tab+8],    3034532234

;-----------------------------------------------------------------------------------------------
;---------WYSWIETLENIE ZAWARTOSCI PAMIECI TABLICY-----------------------------------------------
;-----------------------------------------------------------------------------------------------
        ustaw_kursor 5,5
        cinvoke printf, <'wartosc 0 elementu tablicy = %lx',0>,dword [tab] ; to samo co [tab+0]
        ustaw_kursor 6,5
        cinvoke printf, <'wartosc 1 elementu tablicy = %lx',0>,dword [tab+4] ; to samo co [tab+0] +
        ustaw_kursor 7,5
        cinvoke printf, <'wartosc 2 elementu tablicy = %lx',0>,dword [tab+8] ; to samo co [tab+0]
;        ustaw_kursor 6,5
;        cinvoke printf, <'wartosc 6 elementu tablicy  = %lx',0>,dword [tab+5+4]
;        ustaw_kursor 7,5
;        cinvoke printf, <'wartosc 7 elementu tablicy  = %lx',0>,dword [tab+6+4]
        mov ecx,0
        mov ecx,0
        mov ecx,0
;-----------------------------------------------------------------------------------------------
;---------PETLA PO TABLICY----------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
;     PODCZAS PORUSZANIA SIE PO TABLICY TRZEBA BRAC POD UWAGE WIELKOSC POJEDYNCZEGO ELEMENTU TABLICY
;     DLA DD JEST TO 4 [tab+(ecx*4)]
;-----------------------------------------------------------------------------------------------
;---------PETLA PO TABLICY Z MODYFIKACJA--------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ; ustawienie licznika petli
        mov ecx, [iloscPowtorzen]

    petla:
        ; do eax przesuwamy calkowita ilosc potworzen
        mov edx,[iloscPowtorzen]
        ; odejmujemy wartosc petli od aktualnego licznika petli
        sub edx,ecx
        ; do tablicy przesuwamy wynik i dla n elementu mamy [iloscPowtorzen]-n
        mov [tab+(ecx*4)], edx
        ; koniec petli
    loop petla


        ustaw_kursor 9,5
        cinvoke printf, <'Elementy tablicy'>
        ustaw_kursor 10,5
        ; ustawienie licznika petli
        mov ecx, [iloscPowtorzen]
        ; petla wyswietlajaca wartosci w tablicy
    petla2:
        ; przekazywanie do rejestru edx wartosci tablicy
        mov edx, [tab+(ecx*4)]
        ; wyswietlenie wartosci z tablicy
        wysietl_wartosc edx
;
    loop petla2


;-----------------------------------------------------------------------------------------------
;---------PETLA PO TABLICY BEZ MODYFIKACJI------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ustaw_kursor 13,5
        cinvoke printf, <'Elementy tablicy bez modyfikacji'>
        mov ecx, [iloscPowtorzen]

    petla3:
        ; przesuwanie do tablicy wartosci licznika petli
        mov [tab+(ecx*4)], ecx
        ; koniec petli
    loop petla3


        ustaw_kursor 14,5
        ; ustawienie licznika petli
        mov ecx, [iloscPowtorzen]
        ; petla wyswietlajaca wartosci w tablicy
    petla4:
        ; przekazywanie do rejestru edx wartosci tablicy
        mov edx, [tab+(ecx*4)]
        ; wyswietlenie wartosci z tablicy
        wysietl_wartosc edx
;
    loop petla4



        popad ;zdejmuje ze szczytu stosu wszystkie wartości i kopiuje je do ośmiu rejestrów ogólnego przeznaczenia.
}

macro wysietl_wartosc zmienna
{
        pushad   ;odkłada wszystkie rejestry na stoss

        cinvoke printf, <' %lx',0>,zmienna

        popad ;zdejmuje ze szczytu stosu wszystkie wartości i kopiuje je do ośmiu rejestrów ogólnego przeznaczenia.
}


