;-----------------------------------------------------------------------------------------------------
;       argorytmMnozenia
;-----------------------------------------------------------------------------------------------------
;
;
;
;
;
;
macro argorytmMnozenia arg1, arg2
{
        pushad

;-----------------------------------------------------------------------------------------------
;---------------------CZESC PIERWSZA MNOZENIA---------------------------------------------------
;-----------------------------------------------------------------------------------------------

; MNOZENIE liczby1 arg1 z liczba1 arg2
        mov eax, dword [arg1]         ; do EAX przesuwamy pierwsza liczbe z argumentu 1
        mul dword [arg2]              ; mnozymy liczbe1 z arg1 z liczba1 z arg2
        mov dword [wynik], eax        ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        mov dword [wynik+4], edx      ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

;-----------------------------------------------------------------------------------------------
;---------------------CZESC DRUGA MNOZENIA------------------------------------------------------
;-----------------------------------------------------------------------------------------------

; mnozenie liczby2 arg1 z liczba1 arg2

        mov eax,dword [arg1+4]        ; do eax przenosimy liczbe2 arg1
        mul dword [arg2]              ; mnozymy liczbe2 z arg1 z liczba1 z arg2
        add dword [wynik+4], eax      ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        adc dword [wynik+8], edx      ; dodawanie z flaga carry

        adc dword [wynik+12],0        ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

; mnozenie liczy1 arg1 z liczba2 arg2

        mov eax, dword [arg1]         ; do eax przenosimy liczbe1 arg1
        mul dword [arg2+4]            ; mnozymy liczbe2 z arg2 z liczba1 z arg1
        add dword [wynik+4], eax      ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        adc dword [wynik+8], edx      ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

        adc dword [wynik+12],0        ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE


;-----------------------------------------------------------------------------------------------
;---------------------CZESC TRZECIA MNOZENIA----------------------------------------------------
;-----------------------------------------------------------------------------------------------

; mnozenie liczby3 arg1 z liczba1 arg2

        mov eax, dword [arg1+8]       ; do eax przenosimy liczbe3 arg1
        mul dword [arg2]              ; mnozymy liczbe3 z arg1 z liczba1 z arg1
        add dword [wynik+8], eax      ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        adc dword [wynik+12], edx     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

        adc dword [wynik+16], 0       ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

; mnozenie liczby2 arg1 z liczba2 arg2
        mov eax, dword [arg1+4]       ; do eax przenosimy liczbe2 arg1
        mul dword [arg2+4]            ; mnozymy liczbe2 z arg1 z liczba2 z arg2
        add dword [wynik+8], eax      ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        adc dword [wynik+12], edx     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

        adc dword [wynik+16], 0       ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

; mnozenie liczby1 arg1 z liczba3 arg2
        mov eax, dword [arg1]         ; do eax przenosimy liczbe1 arg1
        mul dword [arg2+8]            ; mnozymy liczbe1 z arg1 z liczba3 z arg2
        add dword [wynik+8], eax      ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        adc dword [wynik+12], edx     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

        adc dword [wynik+16], 0       ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE


;-----------------------------------------------------------------------------------------------
;--------------------CZESC CZWARTA MNOZENIA-----------NAJWIEKSZA--------------------------------
;-----------------------------------------------------------------------------------------------

        ;[wynik+12] = I = A*S + B*R + C*Q + D*P

; mnozenie liczby4 arg1 z liczba1 arg2
        mov eax, dword [arg1+12]      ; do eax przenosimy liczbe4 arg1
        mul dword [arg2]              ; mnozymy liczbe4 z arg1 z liczba1 z arg2
        add dword [wynik+12], eax     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        adc dword [wynik+16], edx     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

        adc dword [wynik+20], 0       ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

; mnozenie liczby3 arg1 z liczba2 arg2
        mov eax, dword [arg1+8]       ; do eax przenosimy liczbe3 arg1
        mul dword [arg2+4]            ; mnozymy liczbe3 z arg1 z liczba2 z arg2
        add dword [wynik+12], eax     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        adc dword [wynik+16], edx     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

        adc dword [wynik+20], 0       ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

; mnozenie liczby2 arg1 z liczba3 arg2
        mov eax, dword [arg1+4]       ; do eax przenosimy liczbe2 arg1
        mul dword [arg2+8]            ; mnozymy liczbe2 z arg1 z liczba3 z arg2
        add dword [wynik+12], eax     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        adc dword [wynik+16], edx     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

        adc dword [wynik+20], 0       ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

; mnozenie liczby1 arg1 z liczba4 arg2
        mov eax, dword [arg1]         ; do eax przenosimy liczbe1 arg1
        mul dword [arg2+12]           ; mnozymy liczbe1 z arg1 z liczba4 z arg2
        add dword [wynik+12], eax     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE
        adc dword [wynik+16], edx     ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

        adc dword [wynik+20], 0       ; !!!!!!!!! PRZEMYSLEC CO TU SIE DZIEJE

;-----------------------------------------------------------------------------------------------
;--------------------CZESC 5 MNOZENIA-----------------------------------------------------------
;-----------------------------------------------------------------------------------------------

; mnozenie liczby4 arg1 z liczba2 arg2
        mov eax, dword [arg1+12]      ; do eax przenosimy liczbe4 arg1
        mul dword [arg2+4]            ; mnozymy liczbe4 z arg1 z liczba2 z arg2
        add dword [wynik+16], eax
        adc dword [wynik+20], edx

        adc dword [wynik+24], 0

; mnozenie liczby3 arg1 z liczba3 arg2
        mov eax, dword [arg1+8]       ; do eax przenosimy liczbe3 arg1
        mul dword [arg2+8]            ; mnozymy liczbe3 z arg1 z liczba3 z arg2
        add dword [wynik+16], eax
        adc dword [wynik+20], edx

        adc dword [wynik+24], 0

; mnozenie liczby2 arg1 z liczba4 arg2
        mov eax, dword [arg1+4]       ; do eax przenosimy liczbe2 arg1
        mul dword [arg2+12]           ; mnozymy liczbe2 z arg1 z liczba4 z arg2
        add dword [wynik+16], eax
        adc dword [wynik+20], edx

        adc dword [wynik+24], 0

;-----------------------------------------------------------------------------------------------
;--------------------CZESC 6 MNOZENIA-----------------------------------------------------------
;-----------------------------------------------------------------------------------------------

; mnozenie liczby4 arg1 z liczba3 arg2
        mov eax, dword [arg1+12]       ; do eax przenosimy liczbe4 arg1
        mul dword [arg2+8]             ; mnozymy liczbe4 z arg1 z liczba3 z arg2
        add dword [wynik+20], eax
        adc dword [wynik+24], edx

        adc dword [wynik+28], 0

; mnozenie liczby3 arg1 z liczba4 arg2
        mov eax, dword [arg1+8]        ; do eax przenosimy liczbe3 arg1
        mul dword [arg2+12]            ; mnozymy liczbe3 z arg1 z liczba4 z arg2
        add dword [wynik+20], eax
        adc dword [wynik+24], edx

        adc dword [wynik+28], 0

;-----------------------------------------------------------------------------------------------
;---------------------CZESC 7 MNOZENIA----------------------------------------------------------
;-----------------------------------------------------------------------------------------------

; mnozenie liczby4 arg1 z liczba4 arg2
        mov eax, dword [arg1+12]       ; do eax przenosimy liczbe4 arg1
        mul dword [arg2+12]       ; mnozymy liczbe4 z arg1 z liczba4 z arg2

        bt [arg1],16

        add dword [wynik+24], eax
        adc dword [wynik+28], edx
        adc dword [wynik+32], 0

       ;; zrobic recznie mnozenie bez najstarszego bitu


;; mnozenie liczby4 arg1 z liczba4 arg2
;        mov eax, dword [arg1+12]       ; do eax przenosimy liczbe4 arg1
;        mul dword [arg2+12]            ; mnozymy liczbe4 z arg1 z liczba4 z arg2
;        add dword [wynik+24], eax
;        adc dword [wynik+28], edx
;        adc dword [wynik+32], 0


        popad
}
