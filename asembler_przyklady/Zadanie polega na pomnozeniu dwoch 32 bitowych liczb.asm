format PE Console 4.0
entry start
include 'include/win32ax.inc'
include 'win_macros.inc'

section '.text' code readable executable
start:
;-----------------------------------------------------------------------------------------------
;---------Wczytywanie liczb---------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ; wywolanie makro z pliku win_macros.inc
        clrscr                          ; czyscimy ekran
        ; wywolanie makro z pliku win_macros.inc
        ustaw_kursor 1,5                ; Ustawienie kursora na pozycję 1,5
        cinvoke printf, <'arg1 = ',0>    ; wyswietlamy napis: 'zm1 = '

        ; jesli chcemy pobrac/wyswietlic liczbe ósemkową wpisujemy %o,
        ; jesli chcemy pobrac/wyswietlic liczbe dziesietna wpisujemy %u
        ; jesli chcemy pobrac/wyswietlic liczbe szesnastkowa wpisujemy %x
        cinvoke scanf, '%x', arg1        ; pobieramy  hexa, ktory jest zapisany do zm1

        ; wywolanie makro z pliku win_macros.inc
        ustaw_kursor 2,5                ; Ustawienie kursora na pozycję 2,5

        cinvoke printf, <'arg2 = ',0>    ; wyswietlamy napis: 'zm2 = '
        cinvoke scanf, '%x', arg2        ; pobieramy  hexa, ktory jest zapisany do zm2


;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ; do debuga

        mov esi,1
        mov esi,1
        mov esi,1
        mov esi,1

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

;-----------------------------------------------------------------------------------------------
;---------Obliczanie----------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------


; Zadanie polega na pomnozeniu dwoch 32 bitowych liczb.
; Jedna liczba/litera w systemie szesnastkowym ma 4 bity, maksymalna liczba to 1111 czyli F
; 32 bity jest to osiem liczb i liter w systemie szesnastkowym
; np FFFF FFFF = maksymalna liczba zapisana na 32 bitach

 ;Slownik:
 ; - liczba/litera - cyfra liczby podanej z klawiatury przez uzytkownika do mnozenia
 ; - obrazek :
 ; wykonujemy mnozenie argumentu 1(ABCD) i argumentu 2 (PQRS)
 ; na rysunku dla argumentow zapisano dwie przykladowe liczby szesnastkowe
 ; pierwsza wykonana operacja mnozenia jest D*S tak wiec z pierwszego i drugiego argumentu pobierane sa dwie ostatnie cyfry.
 ; operacja ta jest powtarzana dla kazdej nastepnej cyfry az do zakonczenia


; Wiadomo, że rejestry procesora maja taka skrukturę: (taki podział odnosi się również do innych, bx, cx, dx, si itd ;)
; Rejestr AX ma 16 bitów
; Rejestr AX sklada się z rejestru AH i AL
; AH w polaczeniu z AL daje nam cały  AX
; Pierwsze 8 bitów rejestru AX to jest AL
; Kolejne 8 bitów rejestru AX to jest AH
; Z racji tego, że niema mniejszego podziału rejestru AL (pamiętamy że al ma 8 bitów), będziemy operować na 2 liczbach i literach w mnożeniu i dodawaniu (a my potrzebujemy na 1 liczbę/literkę 4 bity)


; Pełne 32 bity to jest  EAX -

; sposob wykonywania obliczen i znaczenie poszczegolnych cyfr:
; skoro już przyjeliśmy że będziemy mnożyć i dodawać po 2 liczby i literki, schemat tego konkretnego mnożenia jest przedstawiony na rysunku
; WIĘC PAMIĘTAJ, ZE BIERZEMY PO 2 LICZBY/LITERKI SZESNASTKOWO

; Dlaczego bierzemy po 2? ano dlatego, że gdybyśmy się uparli brać po 1, tak jak jest w poleceniu trzeba robić tak:
; bierzemy najpierw 1 liczbę do rejestru al
; mov  al, byte[arg1]
; zerujemy 2 liczbę która jest zapisana w rejestrze al za pomocą operacji mnożenia bitowego
; czyli 0000 1111 mnożymy bitowo z liczbą podaną co da nam 0000 podana liczba
; AND al, 0Fh

; to samo wykonujemy dla drugiej liczby do której będziemy mnożyć
; mov bl, byte[arg2]
; AND bl, 0Fh

; i teraz możemy wykonać mnożenie dla dwóch pierwszych liczb/literek zapisanych szesnastkowo
; czyli
; mul bl (ta operacja mnoży al z bl)

; w kolejnym kroku/krokach trzeba wziąść z pierwszej liczbny 2 liczbę/literkę
; czyli znów przenosimy do rejestru al dwie liczby/literki
; mov  al, byte[arg1]
; i wykonujemy operację mnożenia bitowego ale na 2 części rejestru al czyli na 2 liczbie/literce
; AND al, F0h
; I tak te operacje wykonujemy po kolei na tych rejestrach, próbująć się nie pomylić i kod dla Ciebie byłby nie czytelny
;
; wykonujemy mnozenie poszczegolnych skladnikow ktore przedstawione sa na rysunku

        ; operacje czyszczenia jest potrzebna bo czasem powstaja jakies smieci w calym rejestrze eax i podczas mnozenia i dodawania dodaja sie dziwne liczby
        AND EAX,0F                                         ; czyscimy rejestr EAX operacja mnozenia przez zero, jest to to samo co mov eax,0
        AND EBX,0F                                         ; czyscimy rejestr EBX operacja mnozenia przez zero, jest to to samo co mov ebx,0


;[wynik] = L = D*S

        mov  al, byte[arg1]          ; AL = d              ; do rejestru al przenosimy 2 pierwsze wprowadzone liczby/literki(np FF) z 1 argumentu ktory podaliśmy
        mov  bl, byte[arg2]          ; BL = s              ; do rejestru bl przenosimy 2 pierwsze wprowadzone liczby/literki(np A1) z 2 argumentu ktory podaliśmy
        mul  bl                      ; d*s = AL*BL         ; wykonujemy mnożenie
        ; aby przenieść rejestr al do zmiennej wynik nalezy wykonac 'konwersje' tego do czego przenosimy z danego rejestru dlatego pojawia sie tam byte (czyli bajt = 8 bitow ktore sa w rejestrze al)
        mov  byte [wynik], al                              ; przesuwamy wynik mnozenia ktory znajduje sie w rejestrze al do wyniku
        ; jesli nastapilo podczas mnozenia wyjscie wyniku poza 8 bitow wtedy wieksza czesc wyniku znajdzie sie w rejestrze ah
        mov  byte [wynik+1], ah                            ; przesuwamy wynik mnozenia ktory znajduje sie w rejestrze ah do wyniku

;[wynik+1]  = K = C*S + D*R

        AND EAX,0F
        AND EBX,0F

        ; aby dobrac sie do kolejnej liczby (a raczej kolejnego zestawu dwoch liczb/liter), trzeba wykonac przesuniecie po 8 bitach na zmiennej
        ; przesuniecie po 8 bitach na wprowadzonej zmiennej jest wykonywane za pomoca +1 (jesli bedziemy chcieli sie dobrac do kolejnych nastepnych to +2 itd)
        ; przesuniecie +1 pominie nam pierwsze z prawej strony 8 bitow(od 0 do 7) i bedziemy od 8 bita pobrac
        ; jak wyzej bedziemy pobierali po 2 (literki/liczby) co na rysunku jest zapisane jako 1 liczba (by nie mieszac!!!)

        ; wykonujemy polecenia analogicznie do tego co wyzej
        ; popatrz na rysunek co aktualnie wykonujemy!!
        mov  al, byte[arg1+1]         ; AL = C
        mov  bl, byte[arg2]           ; BL = S

        mul  bl                       ; bl*al = C*S

        ; wynik musimy zapisac z przesunieciem, bo on rowniez jak widac na rysunku musi byc z przesunieciem, bo inaczej nadpiszemy to co mamy wczesniej wyliczone w 1 kroku
        add  byte [wynik+1], al
        ; jezeli podczas dodawania nastapi "brak miejsca na wynik" musimy je rowniesz uwzglednic i dodac do wyniku
        ; operacja adc wykonuje dodawanie 2 liczb tej ktora jest w ah do wynik+2 z uwzglednieniem przesuniecia, wtedy umieszcza wynik w 1 operandzie ktory zastosowalismy
        adc  byte [wynik+2], ah
        ; jezeli podczas dodawania znow nastapilo "brak miejsca na wynik", wtedy trzeba przesunac do kolejnego pola wynik dodajac go z 0 (dodajemy przesniesienie to co sie nie zmiescilo, "dodajemy takie 1 dalej :D")
        adc  byte [wynik+3], 0

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1]          ; al = D
        mov  bl, byte [arg2+1]        ; BL = R
        mul  bl                       ; bl*al = D*R

        add  byte [wynik+1], al
        adc  byte [wynik+2], ah
        adc  byte [wynik+3], 0


;[wynik+2]  = J = C*R + B*S + D*Q

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+1]        ; al = R
        mov  bl, byte [arg2+1]        ; BL = C
        mul  bl                       ; bl*al = R*C

        add  byte [wynik+2], al
        adc  byte [wynik+3], ah
        adc  byte [wynik+4], 0

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+2]        ; al = B
        mov  bl, byte [arg2]          ; BL = S
        mul  bl                       ; bl*al = B*S

        add  byte [wynik+2], al
        adc  byte [wynik+3], ah
        adc  byte [wynik+4], 0

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1]          ; al = Q
        mov  bl, byte [arg2+2]        ; BL = D
        mul  bl                       ; bl*al = Q*D

        add  byte [wynik+2], al
        adc  byte [wynik+3], ah
        adc  byte [wynik+4], 0


;[wynik+3]  = I = A*S + B*R + C*Q + D*P

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+3]        ; al = A
        mov  bl, byte [arg2]          ; BL = S
        mul  bl                       ; bl*al = A*S

        add  byte [wynik+3], al
        adc  byte [wynik+4], ah
        adc  byte [wynik+5], 0

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+2]        ; al = B
        mov  bl, byte [arg2+1]        ; BL = R
        mul  bl                       ; bl*al = B*R

        add  byte [wynik+3], al
        adc  byte [wynik+4], ah
        adc  byte [wynik+5], 0

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+1]        ; al = C
        mov  bl, byte [arg2+2]        ; BL = Q
        mul  bl                       ; bl*al = C*Q

        add  byte [wynik+3], al
        adc  byte [wynik+4], ah
        adc  byte [wynik+5], 0

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1]          ; al = D
        mov  bl, byte [arg2+3]        ; BL = P
        mul  bl                       ; bl*al = D*P

        add  byte [wynik+3], al
        adc  byte [wynik+4], ah
        adc  byte [wynik+5], 0


;[wynik+4]  = H = A*R + B*Q + C*P

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+3]        ; al = A
        mov  bl, byte [arg2+1]        ; BL = R
        mul  bl                       ; bl*al = A*R

        add  byte [wynik+4], al
        adc  byte [wynik+5], ah
        adc  byte [wynik+6], 0


        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+2]        ; al = B
        mov  bl, byte [arg2+2]        ; BL = Q
        mul  bl                       ; bl*al = B*Q

        add  byte [wynik+4], al
        adc  byte [wynik+5], ah
        adc  byte [wynik+6], 0

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+1]        ; al = C
        mov  bl, byte [arg2+3]        ; BL = P
        mul  bl                       ; bl*al = C*P

        add  byte [wynik+4], al
        adc  byte [wynik+5], ah
        adc  byte [wynik+6], 0


;[wynik+5]  = G = A*Q + B*P

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+3]        ; al = A
        mov  bl, byte [arg2+2]        ; BL = Q
        mul  bl                       ; bl*al = A*Q

        add  byte [wynik+5], al
        adc  byte [wynik+6], ah
        adc  byte [wynik+7], 0

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+2]        ; al = B
        mov  bl, byte [arg2+3]        ; BL = P
        mul  bl                       ; bl*al = B*P

        add  byte [wynik+5], al
        adc  byte [wynik+6], ah
        adc  byte [wynik+7], 0


;[wynik+5]  = F = A*P

        AND EAX,0F
        AND EBX,0F

        mov  al, byte [arg1+3]        ; al = A
        mov  bl, byte [arg2+3]        ; BL = P
        mul  bl                       ; bl*al = A*P

        add  byte [wynik+6], al
        adc  byte [wynik+7], ah


        ; wykonujemy makro ustawiajac kursor
        ustaw_kursor 4,5
        ; wyswietlanie wyniku dla duzych liczb (a takie moga byc), wyglada w nastepujacy sposob
        cinvoke printf, <'wynik = %llx',0>,dword [wynik], dword [wynik+4]           ; wyswietlamy wynik

;-----------------------------------------------------------------------------------------------
;---------SPRAWDZENIE --------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ; sprawdzenie
        mov edx, dword [arg2]                                       ; do rejestru edx przenosimy wartosc wpisana do zmiennej zm2
        mov eax, dword [arg1]                                       ; do rejestru eax przenosimy wartosc wpisana do zmiennej zm1
        mul edx                                                    ; wykonujemy mnozenie edx z eax (polecenie mul mnozy wartosc z eax i parametrem ktory przekazujemy)

        ustaw_kursor 7,5                                           ; ustawienie kursora na 7,5
        cinvoke printf, <'wynik sprawdzajacy = %llx',0>, eax, edx  ; wyswietlamy wynik z dwoch rejestrow eax oraz edx, liczba w mnozenie moze sie nie zmienic w 1 rejestrze dlatego wyswietlamy je z 2 rejestrow
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
;-----------------------------------------------------------------------------------------------
        ustaw_kursor 8,5                                           ; ustawienie kursora na 8,5

        ; polecenie ma na celu zatrzymanie programu po to by program sie nie zamknal i mozna bylo odczytac to co jest na ekranie
        pob_znak                                                   ; wywolanie makro ktore pobiera znak
        ; wszystkie programy musza miec zakonczenie, polecenie zwalnia pamiec i wykonuje potrzebne rzeczy do tego by nie zawiesil sie program
        end_prog                                                   ; wywolanie makro zakonczenia programu

section '.data' data readable writeable

        arg1             dd 0                                       ; deklaracja zmiennej zm1 dword - podwojne slowo  - 4 bajty int ktorej na starcie przypisujemy 0
        arg2             dd 0                                       ; deklaracja zmiennej zm2 dword - podwojne slowo  - 4 bajty int ktorej na starcie przypisujemy 0

        licznikPetli1   dd 0                                       ; deklaracja licznika petli dword - podwojne slowo  - 4 bajty int ktorej na starcie przypisujemy 0
        licznikPetli2   dd 0                                       ; deklaracja 2 licznika petli dword - podwojne slowo  - 4 bajty int ktorej na starcie przypisujemy 0

        wynik           dq 0                                       ; deklaracja wyniku, wynik mnozenia moze byc 2x wiekszy od mnoznej i mnoznika, dlatego deklarujemy poczworne slowo - 8 bajtow

; db - byte        - bajt            - 1 bajt               - max wartosc 255
; dw - word        - slowo           - 2 bajty              - max wartosc 65 535
; dd - dword       - podwojne slowo  - 4 bajty       int    - max wartosc 4 294 967 295
; pw - pword       - potrojne slowo  - 6 bajtow             - max wartosc w hex FFFF FFFF FFFF - 281 474 976 710 655
; dq - qword       - poczworne slowo - 8 bajtow      long
; td - tbyte       -                 - 10 bajtow