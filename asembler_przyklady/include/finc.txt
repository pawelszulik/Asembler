This directory is out of date. 
It is here only for compatibility reasons with some old sources and examples.
The new standart library include files are in "freshlib" directory.
The files inside "include/win32" directory are wrappers for the new include files.
The files inside "include/libs" directory are libraries that are still not converted
to the new portable format. They will be revised and moved to "freshlib" subsequently.

-------------------------------------------------------------------------------------

This directory will contain all
"Fresh standard library" files and
include files with Windows equates.

The path of this directory should be set as "finc" environment
variable or as "finc" key in Fresh.ini file, section [Environment]

/libs - standard Fresh libraries
/components - Installed visual components