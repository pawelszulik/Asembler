; Win32 programming headers (ASCII)

macro module [arg] {}
macro endmodule {}
macro uses [arg] {}

include '%lib%/macros/_uniquelists.inc'
include '%lib%/macros/_struct.inc'
include '%lib%/macros/Win32/_exceptions.inc'
include '%lib%/macros/_stdcall.inc'
include '%lib%/macros/_dispatch.inc'
include '%lib%/macros/Win32/_import.inc'
include '%lib%/macros/Win32/_export.inc'
include '%lib%/macros/_display.inc'
include '%lib%/macros/_itext.inc'
include '%lib%/macros/_globals.inc'

struc CHAR count { rb count }

macro TEXT lbl,[txt] {
  lbl  db  txt
}


struct TBounds
  .x      dd ?
  .y      dd ?
  .width  dd ?
  .height dd ?
ends

include '%lib%/equates/Win32/_exceptions.inc'
include '%lib%/equates/Win32/_KERNEL32.INC'
include '%lib%/equates/Win32/_USER32.INC'
include '%lib%/equates/Win32/_GDI32.INC'
include '%lib%/equates/Win32/_COMCTL32.INC'
include '%lib%/equates/Win32/_COMDLG32.INC'
include '%lib%/equates/Win32/_SHELL32.INC'
include '%lib%/equates/Win32/_WSOCK32.INC'
include "%lib%/equates/_sqlite3.inc"
