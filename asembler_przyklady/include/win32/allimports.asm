 library kernel32,'KERNEL32.DLL',\
          user32,'USER32.DLL',    \
          gdi32,'gdi32.dll',      \
          comctl32,'comctl32.dll',\
          comdlg32,'comdlg32.dll',\
          shell32, 'shell32.dll', \
          ole32,   'ole32.dll',   \
          advapi32,'advapi32.dll',\
          sqlite3, 'sqlite3.dll'


  include '%lib%/imports/Win32/kernel32.inc'
  include '%lib%/imports/Win32/user32.inc'
  include '%lib%/imports/Win32/gdi32.inc'
  include '%lib%/imports/Win32/ComCtl32.inc'
  include '%lib%/imports/Win32/ComDlg32.inc'
  include '%lib%/imports/Win32/Shell32.inc'
  include '%lib%/imports/Win32/ole32.inc'
  include '%lib%/imports/Win32/advapi32.inc'
  include '%lib%/imports/Win32/sqlite3.inc'