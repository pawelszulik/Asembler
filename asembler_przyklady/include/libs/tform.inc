;-------------------------------
; Form messages
;-------------------------------

winmessage FM_AFTERCREATE   ;(0, 0) - message that is send to the main form window, after creation.
winmessage FM_SETCOLOR      ;(color, 0)
winmessage FM_ONIDLE


struct TForm
  .hFont        dd  ?        ; handle of the form font.
  .ModalResult  dd  ?        ; Modal result for modal forms.
  .hPopupMenu   dd  ?        ; popup menu handler
  .clBackground dd  ?        ; Background color.
ends



;***************************************************************
; ShowModal related constants and structures
;***************************************************************
;-------------------------------
;    Modal show flags
;-------------------------------
MSF_HCENTER  =  1  ; On show, center form horizontaly on screen.
MSF_VCENTER  =  2  ; On show, center form verticaly on screen.
MSF_CENTER   =  3  ; On show, center form horizontaly and verticaly on screen.


;-------------------------------
; Modal result ID's
; Some button must have some of
; these values for ID to set
; automatically ModalResult
; Use it as: MODAL_ID + MR_OK
;-------------------------------
;------------------------------------------------
;   Modal result predefined constants
;------------------------------------------------
MODAL_MIN       =  MR_NONE
MR_NONE         =       0
MR_OK           =       IDOK
MR_CANCEL       =       IDCANCEL
MR_ABORT        =       IDABORT
MR_RETRY        =       IDRETRY
MR_IGNORE       =       IDIGNORE
MR_YES          =       IDYES
MR_NO           =       IDNO
MR_CLOSE        =       IDCLOSE
MR_HELP         =       IDHELP
MODAL_MAX       =  MR_HELP

