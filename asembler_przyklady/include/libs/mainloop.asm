;----------------------------------------------
; Main message loop
;----------------------------------------------
Run:
        call    ProcessMessages
        jc      .terminate

if defined AppLib
        invoke  SendMessageA, [hApplication], AM_ONIDLE, 0, 0
else
  if defined hMainForm
        invoke  SendMessageA, [hMainForm], FM_ONIDLE, 0, 0
  end if
end if
        invoke  WaitMessage
        jmp     Run

.terminate:
