;*************************************************************
;* gui.inc
;*
;* This file contains coolmenu/toolbars/accelerators data
;* structures and constants. It not contains any data or
;* code.
;*************************************************************
GUILib = 1

Ctrl = FCONTROL
Alt = FALT
Shift = FSHIFT

FirstMRU = $2000        ; This is the first ID for MRU menu items.
FirstID = $7000         ; This is the first number for command IDs

;**********************************************************************
; This is ACCEL windows structure.                                    *
; WARNING, SDK help file have error in description of this structure. *
; Flags field must be word, not byte.                                 *
;**********************************************************************
struc TAccel AFlags,AKey,aID {
  .Flags dw  AFlags
  .Key   dw  AKey
  .ID    dw  aID

  .size = $ - .Flags
}

virtual at 0
  TAccel TAccel ?,?,?
end virtual


; This is TBBUTTON structure, but with parameters.
struc TToolbarButton {
  .iBitmap   dd ?
  .idCommand dd ?
  .fsState   db ?
  .fsStyle   db ?
  .resword   dw ?
  .dwData    dd ?
  .iString   dd ?
  .size      =  $ - .iBitmap
}

virtual at 0
  TToolbarButton TToolbarButton
end virtual

;***************************************************************
;*  Action/Menu/Buttons structures                             *
;*  This structure describes "Action" with common properties   *
;*  for menu items and toolbar buttons.                        *
;***************************************************************

struct TAction
  .ID           dw      ?
  .Image        dw      ?
  .ptrText      dd      ?
  .TextLen      dw      ?
  .ptrHint      dd      ?
  .ptrAccel     dd      ?
  .OnExecute    dd      ?
ends


; Possible menuitem flags.
mfNormal        = $00
mfSubmenu       = $01
mfSeparator     = $02
mfChecked       = $08
mfGrayed        = MF_GRAYED
mfEnd           = $80
;MF_GRAYED


struct TCoolMenuItem
  .Action       dw      ?       ; index of action in the action array
  .Flags        db      ?
ends


; Possible button flags
bfButton        = $00
bfSeparator     = $01
bfCheck         = $02
bfGroup         = $04
bfDropDown      = $08
bfCheckGroup    = bfGroup or bfCheck
bfAutosize      = $10

bfChecked       = $80                   ; it's not standard. See "gui.asm"
bfDisabled      = $40
bfWrap          = $20


struct TCoolButton
  .Action       dw      ?
  .Flags        db      ?
ends

macro ActionList lblActions, lblAccel, [Name, Image, Text, Accel, strHint, OnExecute ] {
  common
    local Number
    Number = 0
    label lblActions

  forward
    local Caption, CapLen, HotKey, Hint, HintLen

    if defined options.ShowActions and options.ShowActions
      disp 1, 'Defined action:', `Name, '= $', <Name, $10>, $0d
    end if
    Name = Number + FirstID

    dw  Name
    dw  Image
    dd  Caption
    dw  CapLen

    if ~strHint eq NONE
      dd  Hint
    else
      dd  0
    end if

    if ~Accel eq NONE
      dd  HotKey
    else
      dd  0
    end if

    if defined OnExecute
      dd OnExecute
    else
      dd 0
    end if

    Number = Number + 1

  forward
    Caption db Text
    CapLen  = $ - Caption
            db 0

  forward
    if ~strHint eq NONE

      Hint    db strHint,0

    end if

  common
    lblActions#.Number dd Number

    label lblAccel
    Number = 0

  forward
    if ~ Accel eq NONE
      HotKey TAccel FVIRTKEY+FNOINVERT+Accel,Name
      Number = Number + 1
    end if
  common
    lblAccel#.Count dd Number

}



macro CoolMenu lblMenu, [Flags, Action]  {
  common
    local ..menusize

    dw  ..menusize

    label lblMenu word

  forward
    if Action eqtype ''
      local .Text
      dw  rva .Text - rva $           ; if the item is submenu.
    else
      if Flags and (mfSeparator or mfEnd) = 0
        dw  Action - FirstID            ; if the item is action, this is index in action list.
      else
        dw  -1                          ; if the item is separator.
      end if
  end if

    db  Flags

  common
    dw  -2      ; End of CoolMenu structure.

  forward
    if Action eqtype ''
      .Text db Action,0
    end if

  common
    ..menusize = $ - lblMenu
}


macro CoolToolbar lblToolbar, [Flags, Action]       {
  common
    label lblToolbar
  forward
    if (Flags and bfSeparator) = 0
      dw  Action - FirstID            ; if the item is action, this is index in action list.
    else
      dw  -1                          ; if the item is separator.
    end if

    db  Flags
  common
    dw  -2      ; End of CoolToolbar structure.
}
