;***************************************************************
; Template engine related constants and structures
;***************************************************************

winmessage AM_INITWINDOW
winmessage AM_ALIGNCHANGED


wtChild  = 0
wtParent = 1    ; The current window is the parent for next windows until the window with wtEnd flag.
wtEnd    = 2    ; The current window is the last child of the current parent window. The next
                ; window becomes child of the previous parent.


;-------------------------------------------------------------------------
; This is structure describing the windows parent child structure.
;-------------------------------------------------------------------------
struct TWinTemplate
  .flags      db  ?   ;
  .Align      db  ?
  .ptrClass   dd  ?   ; pointer to the classname.
  .ptrText    dd  ?
  .style      dd  ?
  .styleEx    dd  ?
  .ID         dw  ?
  .left       dw  ?
  .top        dw  ?
  .width      dw  ?
  .height     dw  ?
  .WinProc    dd  ?
ends


macro Window  name, flags, walign, class, txt, style, styleEx, ID, left, top, width, height, winproc {
common
  if ~(name eq NONE)
    label name
  end if
  local ..class, ..text

  db   flags
  db   walign
  dd   ..class ; pointer to the classname.
  if txt eqtype ''
    dd   ..text  ; pointer to the text.
  else
    dd  txt
  end if
  dd   style
  dd   styleEx
  dw   ID
  dw   left
  dw   top
  dw   width
  dw   height
  dd   winproc

  ..class text class
  ..text  text txt
}

ComDlgStyle = WS_CAPTION or WS_SYSMENU or WS_DLGFRAME or WS_TABSTOP; or WS_CLIPCHILDREN
ComCtrlStyle = WS_CHILD or WS_VISIBLE or WS_CLIPSIBLINGS


;------------------------------------------------------------------------
; Macro for defining names for window properties (SetProp/GetProp)
; that to be used with and without AddAtom function.
; When the name is not added to the atom table, the first
; dword of the structure contains pointer to the string.
; If the macro is added to the application atom table
; (via CreateProperty procedure), the first dword contains
; atom handle. In both cases GetProp and SetProp uses [propname] as
; pointer to the property name.
;------------------------------------------------------------------------
macro property [name, string] {
  forward
    align 4
    local ..txt
    name  dd ..txt
    ..txt du string, 0
}
