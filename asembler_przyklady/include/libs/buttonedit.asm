

initialize RegisterButtoneditClass
.wc WNDCLASS
begin
        lea     eax, [.wc]
        invoke  GetClassInfoW, [hInstance], cBtnEditPrecursor, eax

        mov     [.wc.lpszClassName], cButtonEditClassName
        mov     eax, [.wc.cbWndExtra]
        mov     [ihButton], eax
        add     eax, 4
        mov     [.wc.cbWndExtra], eax

        mov     eax, [.wc.lpfnWndProc]
        mov     [ptrEditWindowProc], eax
        mov     [.wc.lpfnWndProc], ButtonEditProc

        invoke  GetModuleHandleW, 0
        mov     [.wc.hInstance], eax

        lea     eax,[.wc]
        invoke  RegisterClassW,eax
        return
endp

iglobal
  cBtnEditPrecursor  du 'EDIT', 0
  cBtnEditPrecursor2 du 'BUTTON', 0

  cButtonEditClassName du 'TButtonEdit', 0
endg

uglobal
  ptrEditWindowProc dd ?
  ihButton          dd ?
endg



proc ButtonEditProc, .hwnd, .wmsg, .wparam, .lparam
.rect RECT
begin
        push    ebx esi edi

        mov     ebx, [.wmsg]
        Messages                        \
        WM_CREATE, .wmcreate,           \
        WM_DESTROY, .wmdestroy,         \
        WM_SIZE,    .wmsize,            \
        WM_SETFONT, .wmsetfont,         \
        WM_COMMAND, .wmcommand,         \
\
        BEM_SETBUTTONTEXT, .bemsetbuttontext   ; (0, ptrText)

.default:
;        mov     esi, [ptrEditWindowProc]
        invoke  CallWindowProcW, [ptrEditWindowProc], [.hwnd], [.wmsg], [.wparam], [.lparam]

.finish:
        pop     edi esi ebx
        return

.bemsetbuttontext:
        invoke  GetWindowLongW, [.hwnd], [ihButton]
        stdcall SetControlTextUtf8, eax, [.lparam]
        jmp     .finish

.wmcommand:
        mov     eax, [.wparam]
        cmp     ax, 1
        jne     .default

        shr     eax, 16
        cmp     eax, BN_CLICKED
        jne     .default


.click:
        invoke  GetParent, [.hwnd]
        mov     ebx, eax
        invoke  GetWindowLongW, [.hwnd], GWL_ID
        invoke  SendMessageW, ebx, BEM_BUTTONCLICK, eax, [.hwnd]
        xor     eax, eax
        jmp     .finish

.wmsetfont:
        invoke  GetWindowLongW, [.hwnd], [ihButton]
        invoke  SendMessageW, eax, WM_SETFONT, [.wparam], [.lparam]
        jmp     .default

.wmdestroy:
        invoke  GetWindowLongW, [.hwnd], [ihButton]
        invoke  DestroyWindow, eax
        jmp     .default

.wmcreate:
        invoke  CreateWindowExW, 0, cBtnEditPrecursor2, NULL, \
                                WS_VISIBLE or WS_CHILD or BS_TEXT or BS_VCENTER or BS_NOTIFY,                  \
                                0, 0, 16, 16, [.hwnd], 1, [hInstance], NULL
        mov     ebx, eax
        invoke  SetWindowLongW, [.hwnd], [ihButton], ebx

        invoke  SendMessageW, [.hwnd], WM_GETFONT, 0, 0
        invoke  SendMessageW, ebx, WM_SETFONT, 0, FALSE

        mov     eax, $00100000
        invoke  SendMessageW, [.hwnd], EM_SETMARGINS, EC_RIGHTMARGIN, eax
        jmp     .default


.wmsize:
        invoke  GetWindowLongW, [.hwnd], [ihButton]
        test    eax, eax
        jz      .default

        mov     ebx, eax
        lea     eax, [.rect]
        invoke  GetClientRect, [.hwnd], eax

        mov     esi, [.rect.right]
        sub     esi, [.rect.bottom]

        xor     edi, edi

        invoke  GetWindowLongW, ebx, GWL_STYLE
        test    eax, WS_BORDER
        jnz     @f

        inc     edi
        inc     esi
        sub     [.rect.bottom], 2

@@:
        invoke  SetWindowPos, ebx, 0, esi, edi, [.rect.bottom], [.rect.bottom], SWP_NOZORDER

        mov     eax, [.rect.bottom]
        sub     eax, 2
        and     eax, $ffff
        shl     eax, 16

        invoke  PostMessageW, [.hwnd], EM_SETMARGINS, EC_RIGHTMARGIN, eax
        jmp     .default
endp