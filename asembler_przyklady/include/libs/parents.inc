struct TInternalAlign
  .client        RECT
  .hClientWindow dd ?
  .hDefer        dd ?
ends

;-----------------------------
; Window alignment constants.
;-----------------------------
waNone   = 0
waLeft   = 1
waRight  = 2
waTop    = 3
waBottom = 4
waClient = 5
