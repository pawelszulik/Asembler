
; AsmEdit styles

AES_AUTOINDENT   = 0001h
AES_SMARTTABS    = 0002h
AES_OPTIMALFILL  = 0004h
AES_SECURESEL    = 0008h
AES_AUTOBRACKETS = 0010h
AES_CONSOLECARET = 0020h
AES_INDENTSEL    = 0040h  ; indent/outdent selection by pressing tab/shift+tab

; AsmEdit messages

AEM_SETMODE            = WM_USER + 0   ; (modevalue, null)
AEM_GETMODE            = WM_USER + 1   ; (null, null): modevalue
AEM_SETSYNTAXHIGHLIGHT = WM_USER + 2   ; (syntaxcolors or 0, syntaxproc)
AEM_GETLINEDATA        = WM_USER + 3   ; (linenum, *buffer)
AEM_SETPOS             = WM_USER + 4   ; (*aepos, null)
AEM_GETPOS             = WM_USER + 5   ; (*aepos, null)
AEM_FINDFIRST          = WM_USER + 6   ; (flags, *string): found?
AEM_FINDNEXT           = WM_USER + 7   ; (null, null): found?
AEM_CANFINDNEXT        = WM_USER + 8   ; (null, null): found?
AEM_GETWORDATCARET     = WM_USER + 9   ; (sizeof, *buffer)
AEM_SETTEXTCOLOR       = WM_USER + 10  ; (fg, bg)
AEM_SETSELCOLOR        = WM_USER + 11  ; (fg, bg)
AEM_SETPOPUPMENU       = WM_USER + 12  ; (null, handle)
AEM_SETMARGINCOLOR     = WM_USER + 13  ; (bg, border)
AEM_SETMARGINWIDTH     = WM_USER + 14  ; (gap, width)
AEM_SETTHEME           = WM_USER + 15  ; (null, *aetheme)
AEM_TOGGLEBOOKMARK     = WM_USER + 16  ; (null, null): id
AEM_GOTOBOOKMARK       = WM_USER + 17  ; (id/null, aebm_?): linenum
AEM_CLEARBOOKMARKS     = WM_USER + 18  ; (null, null)
AEM_COMMENT            = WM_USER + 19  ; (null, true/false)
AEM_INDENT             = WM_USER + 20  ; (null, true/false)
AEM_SETLINEDATA        = WM_USER + 21  ; (linenum, *buffer)
AEM_GETLINEPTR         = WM_USER + 22  ; (linenum, 0):lineptr
AEM_GETMODIFIED        = WM_USER + 23  ; (null, null):modified
AEM_SETMODIFIED        = WM_USER + 24  ; (modified, null): old_modified
AEM_SETBOOKMARKICON    = WM_USER + 25  ; (handle, null)
AEM_GETCARETXY         = WM_USER + 26  ; (ptr AECARETXY, 0)
AEM_READONLY           = WM_USER + 27  ; (linenum, 0): previous linenum
AEM_SETFOCUSLINE       = WM_USER + 28  ; (line, null)

; AsmEdit mode flags

AEMODE_OVERWRITE   = 1
AEMODE_VERTICALSEL = 2

AEMODE_NOUPDATE = $200
AEMODE_NOUNDO   = $100

; AsmEdit search flags

AEFIND_CASESENSITIVE = 1
AEFIND_WHOLEWORDS    = 2
AEFIND_BACKWARD      = 4

; AsmEdit bookmark constants

AEBM_NUM  = 0
AEBM_NEXT = 1
AEBM_PREV = 2

; AsmEdit notifications

AEN_NOTIFICATION0 = 3000h
AEN_SETFOCUS     = 3001h
AEN_KILLFOCUS    = 3002h
AEN_TEXTCHANGE   = 3003h
AEN_POSCHANGE    = 3004h
AEN_MODECHANGE   = 3005h
AEN_OUTOFMEMORY  = 300Fh

; AsmEdit position structure

struct AEPOS
   .selectionPosition dd ?
   .selectionLine     dd ?
   .caretPosition     dd ?
   .caretLine         dd ?
ends

struct AECARETXY
  .x0 dd ?
  .y0 dd ?
  .x1 dd ?
  .y1 dd ?
ends

; AsmEdit theme structure
struc AETHEME synproc,tfg,tbg,sfg,sbg,mbg,mfg,mw,gw,focusfg,focusbg,rdonlyfg,rdonlybg,[syn]
{
   common
     .syntax_proc       dd synproc
     .text_color        dd tfg
     .text_background   dd tbg
     .sel_text          dd sfg
     .sel_background    dd sbg
     .margin_background dd mbg
     .margin_border     dd mfg
     .margin_width      dd mw
     .margin_gap        dd gw
     .focus_color       dd focusfg
     .focus_background  dd focusbg
     .rdonly_color      dd rdonlyfg
     .rdonly_background dd rdonlybg
     .syntax_colors:
   forward
     if ~ syn eq
       dd syn
     end if
   common
     .syntax_colors_count = ($ - .syntax_colors) / 4
 }


virtual at 0
  aeTheme AETHEME 0,0,0,0,0,0,0,0,0,0,0,0,0
  sizeof.aeTheme = $
end virtual


aeCharSize = 2  ; how big is one char in the editor.
aeLineCharLen  = $100
aeLineDataLen  = aeLineCharLen * aeCharSize   ; how many bytes in the line.

struct AELINE
  .next dd ?
  .prev dd ?
  .text rw aeLineCharLen
ends

aeBigBlockLen  = $100 * sizeof.AELINE


; AsmEdit offsets in window extra memory.
ofsAsmEditMemory     = 0
