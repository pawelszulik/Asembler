;------------------------------------------------------
; proc JumpTo
; Searches for the message in the message table.
; Arguments:
;   ebx - message number to search in the table.
; Returns:
;   This procedure doesn't returns. It simply
;   jumps to the address from table or imediately
;   after the table if the message is not found.
;   in both cases esi = eip
; Uses:
;   eax, esi
;------------------------------------------------------
proc JumpTo
begin
        pop     esi     ; get table address and remove return
                        ; address from the stack.
.loop:
        lodsd
        test    eax,eax
        jz      .exit

        cmp     bx, ax
        jne     .loop

        sar     eax, 16
        add     esi, eax
.exit:
        jmp     esi
endp



;-------------------------------------------------
; Process all messages in the queue
; then exits. if WM_QUIT is accepted, exits
; directly from application.
;
; This procedure should be called during any
; long process to avoid hanging of application.
;
; Returns CF = 0 if message queue is empty
; Returns CF = 1 is WM_QUIT message was detected.
;         in this case, ProcessMessages also returns
;         the exit code in register eax.
;-------------------------------------------------
proc ProcessMessages
.msg MSG
begin
        push    edi esi ebx

        lea     esi, [.msg]

.msg_loop:
        xor     eax, eax
        invoke  PeekMessageW, esi, eax, eax, eax, PM_REMOVE     ; peek message from queue if any.
        test    eax,eax
        jz      .finish                                 ; exit.

        cmp     [.msg.message], WM_QUIT ; if message is WM_QUIT then quit.
        je      .terminate

; Process keyboard acceleration table if any...
.accel:
if defined AppLib
        invoke  SendMessageW, [hApplication], AM_TRANSACCEL, esi, 0
        test    eax,eax
        jnz     .msg_loop
else
  if (defined hAccelerators) & (defined hMainForm)
          invoke  IsWindowEnabled, [hMainForm]
          test    eax, eax
          jz      .dialog

          invoke  TranslateAcceleratorW, [hMainForm], [hAccelerators], esi
          test    eax,eax
          jnz     .msg_loop
  end if
end if


.dialog:
        invoke  GetAncestor, [.msg.hwnd], GA_ROOT
        invoke  IsDialogMessageW, eax, esi
        test    eax, eax
        jnz     .msg_loop

.translate:
        invoke  TranslateMessage, esi                           ; So, standard processing.
        invoke  DispatchMessageW, esi
        jmp     .msg_loop

.terminate:
        mov     eax, [.msg.wParam]
        stc
        pop     ebx esi edi
        return

.finish:
        clc
        pop     ebx esi edi
        return
endp

