;************************************************
; splitter control structures and constants.
;
; (c)2003 John Found
;************************************************

soNone       = 0
soVertical   = 1
soHorizontal = 2

winmessage SPM_SETCONTROL          ; wparam - handle of the control that will be resized.
winmessage SPM_SETCURSORS          ; wparam - vertical cursor resource, lparam - horizontal cursor resource
winmessage SPM_SETMAXMIN           ; (min, max)

struct TSplitter
  .Width        dd ?        ; width if HSIZE / height if VSIZE
  .Control      dd ?        ; handle of resized control.
  .Hold         POINT
  .Orientation  dd ?      ; Don't set manually this field

  .MinSize      dd ?
  .MaxSize      dd ?

  .crVertical   dd ?      ; Vertical cursor.
  .crHorizontal dd ?      ; Horizontal cursor.
ends

;********************************************

