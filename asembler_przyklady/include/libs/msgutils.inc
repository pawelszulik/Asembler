macro MessageList [message, msghandler] {
  forward
        dw message,msghandler - $ - 2
  common
        dd 0
}

macro Messages [msg, handler] {
if ~ msg eq
      call JumpTo
      MessageList msg, handler
end if
}

..CurrentMessage = WM_USER + $4000

macro winmessage [name] {
  forward
    name = ..CurrentMessage
    ..CurrentMessage = ..CurrentMessage + 1
    if defined options.ShowUserMessages & options.ShowUserMessages
      disp iconInfo, 'User message: ', `name, ' = $', <name, 16>, $0d
    end if
}