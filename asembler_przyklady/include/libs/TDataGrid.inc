struct TDataGrid
  .hwnd      dd  ?      ; handle to the instance window.

; properties
  .ColCount  dd ?
  .RowCount  dd ?
  .ColWidth  dd ?       ; default column width - every column can have different width though.
  .RowHeight dd ?       ; default row height - every row can have diferent height.

  .FixedCols dd ?       ; count of the fixed columns [0..FixedCols-1]
  .FixedRows dd ?       ; count of the fixed rows [0..FixedRows-1]
  .Flags     dd ?       ; grid flags ( dgf* flags)

  .ptrGridData dd ?     ; one pointer for the user.

; Color of the grid elements.
  .clCell         dd ?          ; normal data cell background
  .clFixed        dd ?          ; fixed cell background
  .clSelected     dd ?          ; selected cell background
  .clMarked       dd ?          ; background for the cells that are selected in the moment.
  .clTextCell     dd ?          ; normal cell text color
  .clTextFixed    dd ?          ; fixed cell text color
  .clTextSelected dd ?          ; selected cell text color

  .focused   POINT      ; coordinates of the focused cell.
  .hSelected dd ?       ; handle to region containing selected cells.

; These 2 arrays contains respectively ColCount+1 and RowCount+1 elements.
  .ptrColX dd ?     ; pointer to array of dwords with X coordinates of the columns.
  .ptrRowY dd ?     ; pointer to array of dwords with Y coordinates of the rows.

; back buffer data - allows flicker free drawing of the grid.
  .hShadowDC dd ?
  .hShadow   dd ?
  .fShadowReady dd ?    ; flag that shows whether the shadow need repaint.

  .selection RECT       ; rectangle with current selection.
  .selflags  dd ?       ; 1 - column selection

; Callback functions
  .OnGetPopupMenu dd ? ; proc GetPopupMenu, .ptrGrid, .Col, .Row
  .OnDrawItem dd ?     ; proc DrawItem, .hdc, .ptrRect, .Col, .Row, .ptrData, .fSelected, .fFixed
  .OnGetString  dd ?   ; proc GetData, .Col, .Row, .ptrData, .flags, .fFixed
ends

dgfGridlines = 1
dgfGridLinesFixed = 2


winmessage DGM_SETPARAM     ; (offset, value):nothing
winmessage DGM_GETPARAM     ; (offset, nothing):value

htwXFixed = 1
htwXData  = 2
htwXBoth  = 3

htwYFixed = 1
htwYData  = 2
htwYBoth  = 3